# FreeTubeMerge

A tiny utility for merging [FreeTube](https://freetubeapp.io/) history files. Give it any number of .db history files and it will spit out a file of all the combined watch records and no duplicates.

As FreeTube has no ability to sync watch history or subscriptions between machines, FreeTubeMerge is a slapdash patch to remedy that.

## Syntax

`FreeTubeMerge [OPTIONS] <fileA>.db <fileB>.db ... <fileN>.db`

## History Files

You can export and import history and subscription files from within FreeTube:

*Settings* > *Data Settings*