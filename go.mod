module freetubemerge

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1
	go.uber.org/zap v1.25.0
)

require go.uber.org/multierr v1.10.0 // indirect
