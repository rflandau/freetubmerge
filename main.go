package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"go.uber.org/zap"
	"io"
	olog "log"
	"math"
	"os"
)

// logger
var slog *zap.SugaredLogger

// flags
var fOutPath string
var fDevelopment bool
var fHistory bool
var fSubscriptions bool

func init() {
	flag.StringVar(&fOutPath, "o", "output.db", "Output Path")
	flag.BoolVar(&fDevelopment, "d", false, "Enable development mode? Increases logging")
	flag.BoolVar(&fHistory, "history", false, "Combine history files")
	flag.BoolVar(&fSubscriptions, "subscriptions", false, "NYI")
}

// syntax: FreeTubeMerge FileA FileB
func main() {
	var err error
	var prod bool = !(fDevelopment)
	// establish the logger
	slog, err = GetSugaredLogger(prod)
	if err != nil {
		olog.Fatalln("Failed to generate zap logger. Prod? ", prod, " | error: ", err)
	}
	defer slog.Sync()

	// handle flags
	flag.Parse()
	if len(flag.Args()) < 2 { // must provide at least two files to mergeRecordSets
		fmt.Printf("Syntax: FreeTubeMerge [--subscriptions or --history] [options] FileA FileB ... FileN")
		return
	}

	slog.Debugw("Flag statuses", "Args", flag.Args(), "history", fHistory, "subscription", fSubscriptions)

	// ### branch on history or sub files
	if (!fHistory && !fSubscriptions) || (fHistory && fSubscriptions) {
		fmt.Println("Must specify either --history or --subscriptions")
		return
	}

	// ### open input files
	var files = make([]*os.File, len(flag.Args()))
	for i, name := range flag.Args() {
		files[i], err = os.Open(name)
		if err != nil {
			slog.Errorw("Failed to open file", "file name", name, "error", err)
		}
	}

	var mergedSet map[string]watch
	if fHistory {
		mergedSet = mergeHistory(files)
	} else {
		mergedSet = mergeSubscriptions()
	}
	// marshal the merged hash into a json file, one entry per line
	of, err := os.Create(fOutPath)
	if err != nil {
		slog.Fatalln(err)
	}

	ecdr := json.NewEncoder(of)
	for _, v := range mergedSet {
		if err := ecdr.Encode(v); err != nil {
			slog.Fatalln(err)
		}
	}
}

func mergeHistory(files []*os.File) map[string]watch {
	var parsed = make([]map[string]watch, len(files)) // all files, parsed

	slog.Infow("opening files", "count", len(flag.Args()))
	for i, f := range files {
		parsed[i] = popMap(f)
		slog.Debugw("read file", "file name", f.Name(), "record count", len(parsed[i]))
		if err := f.Close(); err != nil {
			slog.Errorw("Failed to close file", "error", err)
		}
	}

	// ### mergeRecordSets all parsed records
	slog.Infow("merging file records")
	res := mergeRecordSets(parsed)
	slog.Infof("final record count: %d\n", len(res))

	return res
}

func mergeSubscriptions() map[string]watch {
	fmt.Println("NYI")
	os.Exit(0)
	return nil
}

func GetSugaredLogger(prod bool) (s *zap.SugaredLogger, err error) {
	var l *zap.Logger

	if prod {
		if l, err = zap.NewProduction(); err != nil {
			return nil, err
		}
	} else {
		if l, err = zap.NewDevelopment(); err != nil {
			return nil, err
		}
	}
	return l.Sugar(), nil
}

// popMap populates a hashtable(vID -> watch) from the given .json history file
func popMap(f *os.File) map[string]watch {
	var h = make(map[string]watch)

	dcdr := json.NewDecoder(f)
	for {
		var val watch
		if err := dcdr.Decode(&val); err == io.EOF {
			break
		} else if err != nil {
			slog.Fatalw("Failed to decode file", "file", f.Name(), "error", err)
		}
		h[val.VideoID] = val // insert the record into a hash as vID -> watch
	}
	return h
}

// mergeRecordSets merges the two maps, using m1 as a base and looping through m2
// destructive against m1
func mergeRecordSets(recordSets []map[string]watch) map[string]watch {
	// start the result off as the first file to skip a loop
	res := recordSets[0]
	spew.Dump(res)
	// loop through each file, inserting into and updating res with new data
	for i := 1; i < len(recordSets); i++ { // skip the first record
		// loop through the current map for new or updated records
		for vID, record := range recordSets[i] {

			if _, exists := res[vID]; !exists {
				// if the vID does not exist in the result, insert the record
				res[vID] = record
			} else {
				// if the vID does exist, evaluate the new record for new data
				slog.Debugw("evaluate existing record for new data", "vID", vID)
				w := res[vID]
				w.WatchProgress = math.Max(w.WatchProgress, record.WatchProgress)
				res[vID] = w
			}
		}
	}

	return res
}

// watch contains all data for a single history record
type watch struct {
	VideoID              string      `json:"videoId"`
	Title                string      `json:"title"`
	Author               string      `json:"author"`
	AuthorID             string      `json:"authorId"`
	Published            interface{} `json:"published"` // this can come in as absolute milliseconds or "x [time period] ago"
	Description          string      `json:"description"`
	ViewCount            int         `json:"viewCount"`
	LengthSeconds        int         `json:"lengthSeconds"`
	WatchProgress        float64     `json:"watchProgress"`
	TimeWatched          int64       `json:"timeWatched"`
	IsLive               bool        `json:"isLive"`
	Paid                 bool        `json:"paid"`
	Type                 string      `json:"type"`
	ID                   string      `json:"_id"`
	LastViewedPlaylistID string      `json:"lastViewedPlaylistId"`
}
